# Groovy-Quatro
Groovy-Quatro is a PID controller sample firmware for PIC18F46K80, designed and implemented for [Groovy-Quatro](http://www.omiya-giken.com/?page_id=3150).
Mainly two type of controller such as "Open Loop" and "Closed loop".

<img src="http://www.omiya-giken.com/images/Groovy-Quatro/IMG_2345.jpg" width="240" height="320">
<img src="http://www.omiya-giken.com/images/Groovy-Quatro/IMG_3396.jpg" width="320" height="240">


# How to build
The repository snapshot for MPLAB XIDE. 1stly, you should make project, then copied each file to mean project.


# How to use
- Detail is described in [Japanese](http://www.omiya-giken.com/?page_id=3150).
- Detail is described in [English](http://www.omiya-giken.com/?page_id=3150&lang=en).


# What's the application ?
Mecanum-wheel type rover (robot) are typical application as below,  

## Crab
Build-Guide　[Japanese](https://gitlab.com/teamknox/groovy-quatro/-/wikis/Build-Guide:-Project-Crab-(Mecanum-Wheels-Rover)-in-Japanese)/[English](https://gitlab.com/teamknox/groovy-quatro/-/wikis/Build-Guide:-Project-Crab-(Mecanum-Wheels-Rover)-in-English)
 <p>
 <img src="http://www.omiya-giken.com/images/Groovy-Quatro/IMG_3250.jpg" width="320" height="240">

 </p>

# License
Copyright (c) Osamu OHASHI  
Distributed under the MIT License either version 1.0 or any later version. 
