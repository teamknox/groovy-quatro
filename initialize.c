/**
 * @file      initialze.c
 * @author    Osamu OHASHI (Omiya-Giken LLC)
 * ===============================================================
 * Groovy-PID/Quatro (Version 1.2.0)
 * Copyright (c) 2019 Osamu OHASHI (Omiya-Giken LLC)
 * ===============================================================
 * The MIT License : https://opensource.org/licenses/MIT
 *
 * Copyright (c) 2019 Osamu OHASHI (Omiya-Giken LLC)
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

#include "io_define.h"
#include "eeprom.h"
#include "channel.h"
#include "i2c_master.h"

void InitIO()
{
    OSCCON = 0b01110010;    // Internal OSC(16MHz)
     
    ANCON0 = 0b00000000;    // AN0-4  Only Digital-I/O
    ANCON1 = 0b00000000;    // AN8-13 Only Digital-I/O

    TRISA  = 0b00001111;    // RA0-3 = QEI
    TRISB  = 0b00011111;    // RB0-3 = QEI, RB4 = Input trigger for Factory setting
    TRISC  = 0b00011000;    // RC3 = SCA, RC4 = SCL of I2C
    TRISD  = 0b01000000;    // RD7 = Rx of UART2
    TRISE  = 0b00000000;
}

void InitPWM()
{
    CCPTMRS = 0b00000000;   // CCPx uses Timer2.
    T2CON = 0b00000011;     // Set x16 pre-scaler of TMR2
    CCP1CON = 0b00001100;
    CCP2CON = 0b00001100;
    CCP3CON = 0b00001100;
    CCP4CON = 0b00001100;   // PWM
    TMR2 = 0;
    PR2 = 249;
    TMR2ON = 1;
    
    T0CON = 0b11000100;
    TMR0IE = 1;
    TMR0IP = 0;
}

void InitEnc()
{
    INTCON2bits.INTEDG0 = 1;           // RB0 interrupt on raising-edge
    INTCON2bits.INTEDG1 = 1;           // RB1 interrupt on raising-edge
    INTCON2bits.INTEDG2 = 1;           // RB2 interrupt on raising-edge
    INTCON2bits.INTEDG3 = 1;           // RB3 interrupt on raising-edge
     
    INTCON3bits.INT1IP = 1;            // Set RB1 to be high-priority-interrupt
    INTCON3bits.INT2IP = 1;            // Set RB2 to be high-priority-interrupt
    INTCON2bits.INT3IP = 1;            // Set RB3 to be high-priority-interrupt

    INTCONbits.INT0IE = 1;             // Enable interrupt on RB0
    INTCON3bits.INT1IE = 1;            // Enable interrupt on RB1
    INTCON3bits.INT2IE = 1;            // Enable interrupt on RB2
    INTCON3bits.INT3IE = 1;            // Enable interrupt on RB3
    
    RCONbits.IPEN = 1;
    INTCONbits.GIE = 1;
    INTCONbits.GIEH = 1;
    INTCONbits.GIEL = 1;
}

void InitChannel()
{
    PWM[0] = PWM0;
    PWM[1] = PWM1;
    PWM[2] = PWM2;
    PWM[3] = PWM3;
    
    Brake[0] = Brake0;
    Brake[1] = Brake1;
    Brake[2] = Brake2;
    Brake[3] = Brake3;
    
    Forward[0] = Forward0;
    Forward[1] = Forward1;
    Forward[2] = Forward2;
    Forward[3] = Forward3;

    Backward[0] = Backward0;
    Backward[1] = Backward1;
    Backward[2] = Backward2;
    Backward[3] = Backward3;
}

void InitI2C()
{
    I2C_MASTER_init();
}
