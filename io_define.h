/**
 * @file      io_define.h
 * @author    Osamu OHASHI (Omiya-Giken LLC)
 * ===============================================================
 * Groovy-Quatro (Version 1.0.0)
 * Copyright (c) 2019 Osamu OHASHI (Omiya-Giken LLC)
 * ===============================================================
 * The MIT License : https://opensource.org/licenses/MIT
 *
 * Copyright (c) 2019 Osamu OHASHI (Omiya-Giken LLC)
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef IODEFINE_H
#define IODEFINE_H

#include <xc.h>

#define _XTAL_FREQ 16000000   // for __delay functions = 16MHz
#define MAX_CHANNEL 4

#define ASTARISKS "    ***    "
#define PRODUCT_NAME "Groovy-Quatro"
#define VERSION " Ver.1.2.0"
#define COPY_RIGHT " by (c) 2019, 2020 Omiya-Giken LLC"

enum channel {
    LEFT01 = 0,
    LEFT11,
    RIGHT01,
    RIGHT11,
    
    USED_CHANNEL,
};

#define REG_LED LATC1
enum led_onoff {
    LED_ON = 0,
    LED_OFF
};

#define REG_SETTING_PLUG PORTBbits.RB4
enum plug_status {
    PLUG_ON = 0,
    PLUG_OFF
};

#define QEI_PORT    PORTA

#define TEST_PIN LATE0

#define INP1_L  LATC0
#define INP2_L  LATA5
#define INP3_L  LATA6
#define INP4_L  LATA7

#define INP1_R  LATD0
#define INP2_R  LATD1
#define INP3_R  LATD2
#define INP4_R  LATD3

#define FLG_QEIL01_BIT 0x01
#define FLG_QEIL11_BIT 0x02
#define FLG_QEIR01_BIT 0x04
#define FLG_QEIR11_BIT 0x08

// Left-side (@Normal mounting)
#define REG_0_PWM_H CCPR1L
#define REG_0_PWM_M CCP1CONbits.DC1B1
#define REG_0_PWM_L CCP1CONbits.DC1B0
#define REG_1_PWM_H CCPR2L
#define REG_1_PWM_M CCP2CONbits.DC2B1
#define REG_1_PWM_L CCP2CONbits.DC2B0

// Right-side (@Normal mounting)
#define REG_2_PWM_H CCPR3L
#define REG_2_PWM_M CCP3CONbits.DC3B1
#define REG_2_PWM_L CCP3CONbits.DC3B0
#define REG_3_PWM_H CCPR4L
#define REG_3_PWM_M CCP4CONbits.DC4B1
#define REG_3_PWM_L CCP4CONbits.DC4B0

// UART HAL
#define BAUDCONx    BAUDCON2
#define SPBRGx      SPBRG2
#define TXSTAx      TXSTA2
#define TXxIF       TX2IF
#define TXxIP       TX2IP
#define TXxIE       TX2IE
#define TXREGx      TXREG2
#define RCSTAx      RCSTA2
#define RCxIF       RC2IF
#define RCREGx      RCREG2

// SSPCON HAL 
#define SSPxSTAT    SSPSTAT
#define	SSPxADD     SSPADD
#define SSPxCON1    SSPCON1
#define SSPxCON2    SSPCON2
#define SSPxBUF     SSPBUF
#define SSPxCON1bits SSPCON1bits
#define SSPxCON2bits SSPCON2bits
#define SSPxSTATbits SSPSTATbits


#endif //IODEFINE_H

