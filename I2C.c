/**
 * @file      I2C.c
 * @author    Osamu OHASHI (Omiya-Giken LLC)
 * ===============================================================
 * Groovy-Quatro (Version 1.0.0)
 * Copyright (c) 2019 Osamu OHASHI (Omiya-Giken LLC)
 * ===============================================================
 * The MIT License : https://opensource.org/licenses/MIT
 *
 * Copyright (c) 2019 Osamu OHASHI (Omiya-Giken LLC)
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

#include "io_define.h"
#include "I2C.h"

/*
void I2CCheckStartCondition()
{
    SEN = 1;
	while (SEN);
}

void I2CCheckRepeatedStartCondition()
{
    SSPIF = 0;
	RSEN = 1;
	while (RSEN);
}

void I2CCheckStopCondition()
{
    SSPIF = 0;
	PEN = 1;
	while (PEN);
	SSPIF = 0;    
}

unsigned char I2CCheckAck()
{
    unsigned char i2cData;

	if (ACKSTAT) {
        // Acknowledge was not received from slave.
		i2cData = 0xFF;
	} else {
        // Acknowledge was received from slave.
		i2cData = 0x00;
	}

	return(i2cData);    
}

void I2CTransmitAck()
{
    ACKDT = 0;
	ACKEN = 1;
	while (ACKEN);    
}

void I2CTransmitNack()
{
    ACKDT = 1;
	ACKEN = 1;
	while (ACKEN);
}
*/



void I2CTransmitOneByte(unsigned char aByte)
{
	SSPBUF = aByte;
    while(BF);
//    while(ACKSTAT);
//	I2CIdle();
}

void I2CTransmitData(unsigned char aAddress, unsigned char aData)
{
//    I2CIdle();
    SEN = 1;
//    while(SEN);
    I2CTransmitOneByte(aAddress);
    I2CTransmitOneByte(aData);
//    PEN = 1;
//    while(PEN);
}

void I2CTransmitCommand(unsigned char aAddress, unsigned char aRegister, unsigned char aData)
{
    I2CIdle();
    SEN = 1;
    while(SEN);
    I2CTransmitOneByte(aAddress);
    I2CTransmitOneByte(aRegister);
    I2CTransmitOneByte(aData);
    PEN = 1;
    while(PEN);
}

unsigned char I2CReceiveOneByte(unsigned char aAnswerType)
{
    unsigned char data;
    
    ACKDT = aAnswerType;
    RCEN = 1;
    while(!BF);
    ACKEN = 1;
    data = SSPBUF;
    SSPOV = 0;
    I2CIdle();

	return(data);
}

void I2CReceiveData(unsigned char aAddress, unsigned char aDataCount)
{
    unsigned char i;
    
    I2CIdle();
    SEN = 1;
    while(SEN);
    I2CTransmitOneByte(aAddress);
    for (i = 0;i < aDataCount;i++){
        gI2CData[i] = I2CReceiveOneByte(0);
    }
    gI2CData[i] = I2CReceiveOneByte(1);
    PEN = 1;
    while(PEN);
}


void I2CIdle()
{
    while(SEN);
    while(PEN);
    while(RCEN);
    while(ACKEN);
    while(R_NOT_W);
}